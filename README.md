# ![Logo](icons/Gartoon_mimetypes_qif.png)  Csv2Qif



Vos fichiers .qif de votre banque ne sont pas de bonne qualité ? Vous rencontrez des problèmes pour l'importation des données dans votre logiciel de finances personnelles ? Ce logiciel est fait pour vous.



Ce programme permet de convertir les fichiers .csv de votre banque en .qif de bonne qualité pour qu'ils soient par la suite parfaitement exploitables par des logiciels tels que [Kmymoney](https://kmymoney.org/), [Homebank](http://homebank.free.fr/), [Skrooge](https://skrooge.org/), [Gnucash](http://gnucash.org/), ...

Pour lancer le logiciel, il suffit de taper la commande suivante 

`python3 gui.py`

La fenêtre suivante s'affichera

![logiciel](demo.png)

Il suffit alors de sélectionner le fichier .csv de votre banque et de lancer la conversion du fichier en .qif.

Ce logiciel fonctionne à l'aide des bibliothèques csv et PyQt5, veuillez vérifier qu'ils soient bien installés sur votre ordinateur en cas de dysfonctionnement.


Merci à [Wikimedia.org](https://commons.wikimedia.org) pour fournir les logos de :  
 * du logiciel [Lien vers le logo](https://commons.wikimedia.org/wiki/File:Gartoon_mimetypes_qif.svg)  
 * de [Boursorama](https://commons.wikimedia.org/wiki/File:Boursorama.jpg)  
 * de [N26](https://commons.wikimedia.org/wiki/File:N26_(Direktbank)_2018_logo.svg)  
 * du [Crédit Mutuel de Bretagne](https://fr.wikipedia.org/wiki/Fichier:Logo_Cr%C3%A9dit_Mutuel_de_Bretagne_CMB.svg)  
 * et du [Crédit Agricole du Morbihan](https://commons.wikimedia.org/wiki/File:Logo_Credit_Agricole.png)  