#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  2 20:07:48 2019

@author: antoine
"""

# Info about .qif format
# https://en.wikipedia.org/wiki/Quicken_Interchange_Format

import os
import csv
import datetime


class Transaction():
    "to describe a transaction found in the csv file"

    def __init__(
            self,
            QDate,
            QAmount,
            QMemo,
            QPayee,
            QCategory,
            QSubCategory = None):
        self.QDate = QDate
        self.QAmount = QAmount
        self.QMemo = QMemo
        self.QPayee = QPayee
        self.QCategory = QCategory
        self.QSubCategory = QSubCategory
        
    def get_LCategory(self):
        "return Category:SubCategory"
        if self.QSubCategory is None:
            return self.QCategory
        else:
            return self.QCategory + ":" + self.QSubCategory

    def __repr__(self):
        txt = ("Date : {} \n"
               "Amount : {} \n"
               "Memo : {} \n"
               "Payee : {} \n"
               "Category : {} \n").format(
                       self.QDate,
                       self.QAmount,
                       self.QMemo,
                       self.QPayee,
                       self.get_LCategory())
        return txt

    def get_qif(self, include_categorie=True, replace_empty_payee=True):
        "return the transaction into qif format"
        txt = ("D" + self.QDate,
               "T" + str(self.QAmount),
               "M" + self.QMemo)
        
        if replace_empty_payee and self.QPayee=="":
            Payee=self.QMemo
        else:
            Payee=self.QPayee
        txt+=("P" + Payee,)
            
        if include_categorie:
            txt += ("L" + self.get_LCategory(),)            
        return '\n'.join(txt) + "\n^"


class Transactions():
    "to describe all the transaction found in the csv file"

    def __init__(self, file, bank = "BSR"):
        self.file = file
        self.bank = bank
        self.transactions = []
        self.read_csv()
        
        # filename
        # /home/antoine/Bureau/Qif/export-operations-02-01-2019_19-02-56.csv
        print("Filename : ", self.file)
        # base export-operations-02-01-2019_19-02-56.csv
        base = os.path.basename(self.file)
        print("Base : ", base)
        # name export-operations-02-01-2019_19-02-56
        name = os.path.splitext(base)[0]
        print("Name : ", name)
        # qif file
        # /home/antoine/Bureau/Qif/export-operations-02-01-2019_19-02-56_correct.qif
        self.qif_filename = os.path.splitext(self.file)[0] + ".qif"
        print("Qif filename : ", self.qif_filename)
        # qif name export-operations-02-01-2019_19-02-56_correct.qif
        self.qif_name = os.path.basename(self.qif_filename)
        print("Qif name : ", self.qif_name)
        
        print("Bank : ", self.bank)
        
    def read_csv(self):
        "read csv file and return transations"
        getattr( self , 'read_csv_%s' % self.bank )()

    def read_csv_CMB(self):
        "read csv file and return transations form CMB csv file"
        with open(self.file, 'r', encoding='UTF8') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';')
            next(spamreader) # pour sauter la premiere ligne

            for row in spamreader:
                date_op, date_val, reference, debit_montant_eur, credit_montant_eur = row
                
                if credit_montant_eur == "":
                    montant_eur = -float(debit_montant_eur.replace(",","."))
                else :
                    montant_eur = float(credit_montant_eur.replace(",","."))                
                QDate = date_val
                QMemo = reference
                QAmount = montant_eur
        
                transaction = Transaction(
                        QDate,
                        QAmount,
                        QMemo,
                        QPayee = "",
                        QCategory = "",
                        QSubCategory = None
                        )
                print(transaction)
                # print(transaction.get_qif())
                self.transactions.append(transaction)
                
    def read_csv_CA56(self):
        "read csv file and return transations form CA56 csv file"
        with open(self.file, 'r', encoding='ISO-8859-14') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';')
            
            # pour sauter les 11 premieres lignes
            for row in range(1,12):
                next(spamreader) 
                
            for row in spamreader:
                              
                try :
                    date, reference, debit_montant_eur, credit_montant_eur, crap = row
                except :
                    try :
                        date, reference, debit_montant_eur, credit_montant_eur = row
                    except :
                        break
                
                if credit_montant_eur == "":
                    montant_eur = -float(debit_montant_eur.replace(",","."))
                else :
                    montant_eur = float(credit_montant_eur.replace(",","."))
                
                QDate = date
                QMemo = reference.replace("\n","")
                QAmount = montant_eur
        
                transaction = Transaction(
                        QDate,
                        QAmount,
                        QMemo,
                        QPayee = "",
                        QCategory = "",
                        QSubCategory = None
                        )
                print(transaction)
                # print(transaction.get_qif())
                self.transactions.append(transaction)
                
    def read_csv_N26(self):
        "read csv file and return transations form N26 csv file"
        with open(self.file, 'r', encoding='UTF8') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
            next(spamreader) # pour sauter la premiere ligne
            for row in spamreader:
                date, beneficiaire, num_compte, type_transaction, reference, \
                montant_eur, montant_devise, select_devise, taux_conversion = row
                
                QDate = datetime.datetime.strptime(date, '%Y-%m-%d').strftime("%d/%m/%Y")
                QMemo = reference
                QCategory = ""
                QPayee = beneficiaire.title()
                QAmount = montant_eur
        
                transaction = Transaction(
                        QDate,
                        QAmount,
                        QMemo,
                        QPayee,
                        QCategory,
                        QSubCategory = None
                        )
                # print(transaction)
                # print(transaction.get_qif())
                self.transactions.append(transaction)
                
    def read_csv_BSR(self):
        "read csv file and return transations from BSR csv file"
        with open(self.file, 'r', encoding='ISO-8859-14') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
            next(spamreader) # pour sauter la premiere ligne
            for row in spamreader:
                #print (', '.join(row))
                dateOp_tmp, dateVal_tmp, label, category, categoryParent, supplierFound, amount, comment, accountNum, accountLabel, accountBalance = row
                dateOp = datetime.datetime.strptime(dateOp_tmp, '%Y-%m-%d').strftime("%d/%m/%Y")
                #dateVal = datetime.datetime.strptime(dateVal_tmp, '%Y-%m-%d').strftime("%d/%m/%Y")
                QDate = dateOp
                QAmount = amount.replace(",", ".").replace(" ", "")
                QMemo = label
                QPayee = "" #supplierFound.title()
                QCategory = categoryParent.title()
                QSubCategory = category.title()
                transaction = Transaction(
                        QDate,
                        QAmount,
                        QMemo,
                        QPayee,
                        QCategory,
                        QSubCategory
                        )
                # print(transaction)
                # print(transaction.get_qif())
                autorisation_en_cours=False
                if dateVal_tmp=="":
                    autorisation_en_cours=True
                if not(autorisation_en_cours):
                    self.transactions.append(transaction)

    def __repr__(self):
        txt = ""
        for transaction in self.transactions:
            txt += str(transaction) + "\n"
        return txt

    def get_qif(self, include_categorie=True, replace_empty_payee=True):
        "return all the transactions in qif format"
        txt = "!Type:Bank\n"
        for transaction in self.transactions:
            txt += str(transaction.get_qif(include_categorie, replace_empty_payee)) + "\n"
        return txt
    
    def write_qif(self, include_categorie = True, replace_empty_payee=True):
        "write qif file"
        fichier = open(self.qif_filename, "w")
        print("Include category : ", include_categorie)
        print("Replace empty payee : ", replace_empty_payee)
        fichier.write(str(self.get_qif(include_categorie, replace_empty_payee)))
        fichier.close()

if __name__ == '__main__':
    file = "/home/antoine/Bureau/export-operations-31-07-2021_10-17-46.csv"
    transactions = Transactions(file, "BSR")
    print(transactions.get_qif(False, True))
    #transactions.write_qif(True)
